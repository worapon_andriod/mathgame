package buu.worapon.mathgame

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import buu.worapon.mathgame.databinding.FragmentMainBinding

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER


/**
 * A simple [Fragment] subclass.
 * Use the [MainFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class MainFragment : Fragment() {
    // TODO: Rename and change types of parameters

    private  var menu =0
    private var pointCorrect = 0
    private var pointIncorrect = 0


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding = DataBindingUtil.inflate<FragmentMainBinding>(inflater, R.layout.fragment_main,container,false)

        pointCorrect = MainFragmentArgs.fromBundle(requireArguments()).pointCorrect
        pointIncorrect = MainFragmentArgs.fromBundle(requireArguments()).pointIncorrect

        binding.apply {
            btnPlus.setOnClickListener {
                menu = 1
                view?.findNavController()?.navigate(MainFragmentDirections.actionMainFragmentToCalculateFragment(pointCorrect,pointIncorrect,menu))
            }
            btnMinus.setOnClickListener {
                menu = 2
                view?.findNavController()?.navigate(MainFragmentDirections.actionMainFragmentToCalculateFragment(pointCorrect,pointIncorrect,menu))
            }
            btnMulti.setOnClickListener {
                menu = 3
                view?.findNavController()?.navigate(MainFragmentDirections.actionMainFragmentToCalculateFragment(pointCorrect,pointIncorrect,menu))
            }
            btnDivide.setOnClickListener {
                menu = 4
                view?.findNavController()?.navigate(MainFragmentDirections.actionMainFragmentToCalculateFragment(pointCorrect,pointIncorrect,menu))
            }
            txtMenuPointCorrect.text = pointCorrect.toString()
            txtMenuPointIncorrect.text = pointIncorrect.toString()

        }



        return binding.root
    }


}