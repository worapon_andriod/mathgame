package buu.worapon.mathgame

import android.graphics.Color
import android.os.Bundle
import android.support.v4.media.session.MediaSessionCompat.Token.fromBundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.activity.addCallback
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import buu.worapon.mathgame.databinding.FragmentCalculateBinding
import kotlinx.android.synthetic.main.fragment_calculate.*
import kotlin.random.Random

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER


/**
 * A simple [Fragment] subclass.
 * Use the [CalculateFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class CalculateFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var pointIncorrect = 0
    private var pointCorrect = 0
    private var menu = 0



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        val binding = DataBindingUtil.inflate<FragmentCalculateBinding>(
            inflater,
            R.layout.fragment_calculate,
            container,
            false
        )
        menu = CalculateFragmentArgs.fromBundle(requireArguments()).menu
        pointCorrect = CalculateFragmentArgs.fromBundle(requireArguments()).pointCorrect
        pointIncorrect = CalculateFragmentArgs.fromBundle(requireArguments()).pointIncorrect
        setPoint(binding)
        play(binding)

        requireActivity().onBackPressedDispatcher.addCallback(this){
            view?.findNavController()?.navigate(CalculateFragmentDirections.actionCalculateFragmentToMainFragment(pointCorrect,pointIncorrect))
        }


        return binding.root

    }

    private fun play(binding: FragmentCalculateBinding) {
        val result = setQuestion(binding)
        val btn1 = binding.btn1
        val btn2 = binding.btn2
        val btn3 = binding.btn3

        randomButton(result, btn1, btn2, btn3)

        checkClick(btn1, result, binding)
        checkClick(btn2, result, binding)
        checkClick(btn3, result, binding)
    }


    private fun checkClick(
        btn: Button,
        result: Int,
        binding: FragmentCalculateBinding
    ) {
        btn.setOnClickListener {
            if (btn.text.toString().toInt() == result) {
                ansCorrect(txtAnswer)
            } else {
                ansIncorrect(txtAnswer)
            }
            setPoint(binding)
            play(binding)

        }
    }

    private fun setPoint(binding: FragmentCalculateBinding) {
        var txtPointCorrect = binding.txtPointCorrect
        var txtPointIncorrect = binding.txtPointIncorrect
        txtPointCorrect.text = pointCorrect.toString()
        txtPointIncorrect.text = pointIncorrect.toString()
    }


    private fun ansIncorrect(txtAnswer: TextView) {
        txtAnswer.text = getString(R.string.incorrectTxt)
        txtAnswer.setTextColor(Color.parseColor("#ff0000"))
        pointIncorrect+=1
    }

    private fun ansCorrect(txtAnswer: TextView) {
        txtAnswer.text = getString(R.string.correctTxt)
        txtAnswer.setTextColor(Color.parseColor("#00ff00"))
        pointCorrect +=1
    }

    private fun randomButton(
        result: Int,
        btn1: Button,
        btn2: Button,
        btn3: Button
    ) {
        val randomNum = Random.nextInt(1, 4)
        val btnValue1 = (result + 1).toString()
        val btnValue2 = (result - 1).toString()
        if (randomNum == 1) {
            btn1.text = result.toString()
            btn2.text = btnValue1
            btn3.text = btnValue2

        } else if (randomNum == 2) {
            btn1.text = btnValue1
            btn2.text = result.toString()
            btn3.text = btnValue2
        } else {
            btn1.text = btnValue1
            btn2.text = btnValue2
            btn3.text = result.toString()
        }
    }


    private fun setQuestion(binding: FragmentCalculateBinding): Int {
        val num1 = binding.num1
        val num2 = binding.num2
        val txtSign = binding.txtSign
        var result = 0

        var number1 = Random.nextInt(0, 10)
        var number2 = Random.nextInt(0, 10)


        if (menu == 1){
            result = number1 + number2
            var mod = 0
            txtSign.text = getString(R.string.plus)

        }else if(menu == 2){
            result = number1 - number2
            txtSign.text = getString(R.string.minus)
        }else if (menu == 3) {
             result = number1 * number2
            txtSign.text = getString(R.string.multiple)
        }else {
            while (true){
                number2 = Random.nextInt(1,10)
//                Log.i("divide",number1.toString())
//                Log.i("divide2",number2.toString())
                if (number1 % number2 ==0){
                    result = number1 / number2
                    txtSign.text = getString(R.string.divide)
                    break
                }
            }
        }
        num1.text = number1.toString()
        num2.text = number2.toString()
        return result
    }




}